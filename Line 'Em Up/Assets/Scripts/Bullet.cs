﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public Rigidbody2D rb;

    public float velocity;

    public int damage;

    public int playerId;

    public int teammateId;

    public Team team;

    public GameObject BlackParticle;
    public GameObject RedParticle;

    void FixedUpdate() {

            transform.Translate(Vector3.up * velocity * Time.fixedDeltaTime, Space.Self);
        
    }

    void OnTriggerEnter2D(Collider2D col) {
    
        PlayerData player = col.gameObject.GetComponent<PlayerData>();

        bool hitItem = col.gameObject.GetComponent<Item>() != null;

        if (player != null) {

            if (player.playerId != playerId && player.playerId != teammateId) {

                //Send damage and the vector between the bullet and player to the playerdata script
                //This vector is sent through the playerdata script and passed to the playercontroller script
                player.TakeDamage(damage, transform.up.normalized);
                Instantiate(RedParticle, transform.position, transform.rotation);
                SimplePool.Despawn(gameObject);

            }

        } else if (!hitItem) {

            Instantiate(BlackParticle, transform.position, transform.rotation);
            SimplePool.Despawn(gameObject);

        }

    }

}