﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{

    [HideInInspector]
    public ItemSpawner spawner;
    [HideInInspector]
    public ItemSpawnPoint spawnPoint;

    protected void DestroyItem()
    {
        if (spawner != null)
        {

            spawner.ItemPickedUp(spawnPoint);

        }

        SimplePool.Despawn(gameObject);

    }

}
