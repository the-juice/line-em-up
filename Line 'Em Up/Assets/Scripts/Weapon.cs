﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    Transform objectHolder;

    public GameObject bulletPrefab;

    public PlayerData playerData;

    public Transform firePoint;

    public bool CanFire = false;

    public Color TeamColor;

    //Reference to audio player
    public AudioScript AudioPlayer;

    //Reference to animator
    public Animator Animator;

    //The weapon's fire rate in shots/second
    public float fireRate;

    //How long it takes for the weapon to reload. Inverse of fire rate
    public float fireTime;
    public float fireTimer;

    public int damage;

    void Start() {

        objectHolder = GameObject.FindGameObjectWithTag("Holder").transform;

        fireTime = 1.0f / fireRate;
        fireTimer = fireTime;

    }

  
    void FixedUpdate() {
        if (CanFire)
        {
            if (fireTimer <= 0)
            {

                fireTimer = fireTime;
                Fire();

            }
            else
            {

                fireTimer -= Time.fixedDeltaTime;

            }
        } else {
            fireTimer -= Time.fixedDeltaTime;

            if (fireTimer <= 0.5)
            {
                fireTimer = 0.05f;
            }
        }
        

    }

    void Fire() {

        Bullet bullet = SimplePool.Spawn(bulletPrefab, firePoint.position, firePoint.rotation).GetComponent<Bullet>();
        bullet.GetComponent<SpriteRenderer>().color = TeamColor;
        bullet.transform.SetParent(objectHolder);
        bullet.damage = damage;
        bullet.playerId = playerData.playerId;
        bullet.teammateId = playerData.teammate.playerId;
        bullet.team = playerData.Team;
        AudioPlayer.playBullet();
        Animator.SetTrigger("Shoot");

    }

}
