﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;

public class PlayerData : MonoBehaviour {

    //Player's unique ID
    public int playerId;
    static int nextid;

    //Player's health, if below 0 the player is dead
    public int health;

    //The max health the player can have
    public int maxHealth;

    //Is the player zipping to their teammate
    public bool isZipping;

    //How long the player stays weakened
    public float weakenedTime = 5f;

    //Reference to the the player's health bar
    public Slider HealthBar;

    //This player's Team
    public Team Team;

    //Opposing team
    Team enemyTeam;

    //Reference to you teammate's PlayerData,
    //which in turn references their gameObject and PlayerController
    public PlayerData teammate;

    //Reference to the player's weapon
    public Weapon weapon;

    //Reference to the text that flashes when you are weakened
    Flash_Weakened FW_Script;

    //Variable for if the player should take damage
    public bool CanTakeDamage = true;

    //Keeps track of whether this player is weakened
    public bool AmWeakened = false;

    //Get a reference to the player controller script
    PlayerController playerController;

    //Get a reference to the collider - used in the Kill() function
    CircleCollider2D mycollider;

    //Create Variables for slowing down time when a player is killed
    public float timeSlowAmount;
    public float timeSlowDuration;

    //Create a reference to the death explosion particle system
    public ParticleSystem deathExplosion;

    //Track when this player is killed
    public bool AmKilled;

    //Create a reference to the becomeWeakened Coroutine - this allows us to stop it mid-delay
    Coroutine becomeWeakenedCo;

    //Put an audio player in the "players" object just to play the death transient sound - since that sound is activated
    //from this prefab as it despawns, that's the easiest way to make the sound play
    public AudioScript DeathTransientPlayer;

    void Start() {

        playerId = nextid;
        nextid++;

        playerController = GetComponent<PlayerController>();
        playerController.playerData = this;

        FW_Script = GetComponentInChildren<Flash_Weakened>();

        mycollider = GetComponent<CircleCollider2D>();

        if(Team == Team.Red) {
            enemyTeam = Team.Green;
        } else if(Team == Team.Green) {
            enemyTeam = Team.Red;
        } else {
            enemyTeam = Team.None;
        }

        Initialize();

    }

    //Resets the player to default values after respawn
    public void Initialize(bool isInitialSpawn = false)
    {

        health = maxHealth;
        HealthBar.value = health;
        isZipping = false;
        mycollider.isTrigger = false; //Restore physics collision (if the player was killed by zipping)
        AmWeakened = false;
        FW_Script.EndFlash();
        //FW_Script.IsActive = false; //Remove the flashing sign
        CanTakeDamage = true;
        AmKilled = false;

        playerController.Inititialize(isInitialSpawn);

    }

    public void TakeDamage(int damage, Vector2 damageVector) {

        if (CanTakeDamage)
        {
            health -= damage;
            //Feed the bullet vector to the playercontroller script to apply knockback
            playerController.TakeDamage(damageVector);
            HealthBar.value = health;
            if (health <= 0)
            {
                health = 0;
                CanTakeDamage = false;
                becomeWeakenedCo = StartCoroutine(becomeWeakened());

            }
        }
    }

    public void RestoreHealth(int amount)
    {
        health += amount;

        if(health > maxHealth)
        {
            health = maxHealth;
        }

        if (AmWeakened)
        {
            becomeStrong();
        }

        HealthBar.value = health;

        playerController.AudioPlayer.playHealthSound();
    }

    IEnumerator becomeWeakened()
    {

        AmWeakened = true;
        playerController.becomeWeakened();
        FW_Script.StartFlash(weakenedTime);
        //await Task.Delay(5000);
        yield return new WaitForSeconds(weakenedTime);
        becomeStrong();
        QuickKill(true);

    }

    public void becomeStrong()
    {
        StopCoroutine(becomeWeakenedCo);
        AmWeakened = false;
        playerController.becomeStrong();
        FW_Script.EndFlash(); //Remove the flashing sign
        CanTakeDamage = true;
    }

    //Kill the player without the time dialation stuff
    public void QuickKill(bool killedByEnemy = false)
    {
        //If killedByEnemy is true then increment the enemy teams score
        //This is toggleable in case you want to kill the players without boosting the score
        if (killedByEnemy) {
            GameManager.Instance.IncrementScore(enemyTeam, 1);
        }

        AmKilled = true;
        mycollider.isTrigger = true;
        DeathTransientPlayer.playDeathTransient();
        playerController.Kill();
        //SpawnController.Instance.Respawn(gameObject);
    }

    public void Kill() //Creating a function that starts the kill() coroutine since the coroutine can't be called directly from
                         //the player controller for some reason. I don't really know how IEnumerators function I'm just trying to get a delay working
    {
        StartCoroutine(KillMe());
    }

    IEnumerator KillMe()
    {
        AmKilled = true;
        Metadesc.CameraShake.ShakeManager.I.AddShake("ShakeHuge"); //Play screen shake
        mycollider.isTrigger = true; //Remove physics collision so the killing player passes through this player's body
        Time.timeScale = timeSlowAmount;  //Slow down time for 2 seconds
        Time.fixedDeltaTime = timeSlowAmount * 0.02f; //Decrease physics update time by the same factor
        //await Task.Delay(timeSlowDurationMs);
        yield return new WaitForSeconds(timeSlowDuration*timeSlowAmount);
        Time.timeScale = 1f;
        Time.fixedDeltaTime = 0.02f; //Restore physics update time to the default of 0.02 s
        GameManager.Instance.IncrementScore(enemyTeam, 1); //Increment enemy's score
        playerController.Kill();
        //yield return new WaitForSeconds(0.5f);
        //SpawnController.Instance.Respawn(gameObject);  //Queue the player for respawn
    }

}
