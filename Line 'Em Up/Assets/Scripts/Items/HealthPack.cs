﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class HealthPack : Item
{

    public int healAmount = 100;

    private void OnTriggerEnter2D(Collider2D collision)
    {

        PlayerData player = collision.gameObject.GetComponent<PlayerData>();

        if(player != null && player.health < player.maxHealth)
        {

            player.RestoreHealth(healAmount);

            DestroyItem();

        }

    }

}
