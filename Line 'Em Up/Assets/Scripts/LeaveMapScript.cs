﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaveMapScript : MonoBehaviour
{


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            SpawnController.Instance.Respawn(other.gameObject);
        }
        
    }
}
