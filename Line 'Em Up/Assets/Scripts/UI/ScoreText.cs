﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreText : MonoBehaviour {

    public TextMeshProUGUI teamOneText;
    public TextMeshProUGUI teamTwoText;

    void Start() {

        GameManager.Instance.RegisterUpdateScore(UpdateScore);

        UpdateScore(0, 0);

    }

    public void UpdateScore(int scoreOne, int scoreTwo) {

        teamOneText.text = scoreOne.ToString();
        teamTwoText.text = scoreTwo.ToString();

    }
}
