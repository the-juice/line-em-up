﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimeText : MonoBehaviour {

    TextMeshProUGUI text;

    void Start() {

        text = GetComponent<TextMeshProUGUI>();

        GameManager.Instance.RegisterUpdateTime(UpdateTime);

        UpdateTime(0f);

    }

    public void UpdateTime(float time) {

        int mins = Mathf.RoundToInt(time) / 60;
        int secs = Mathf.RoundToInt(time) % 60;

        text.text = string.Format("{0}:{1,2}", mins, secs.ToString("00"));

    }

}
