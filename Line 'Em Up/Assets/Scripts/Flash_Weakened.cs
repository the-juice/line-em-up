﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using TMPro;

public class Flash_Weakened : MonoBehaviour
{

    //Time between flashes (in seconds)
    public float flashTime = 0.250f;
    int flashTimeMilliseconds;
    float flashTimer;
    float countertimer;

    public AudioScript AudioPlayer;

    public SpriteRenderer WeakenedText;

    public TextMeshProUGUI timeText;

    bool IsActive = false;

    bool displayed = false;

    void Start()
    {

        flashTimeMilliseconds = (int)(flashTime * 1000);

        WeakenedText.enabled = false;
        timeText.enabled = false;
    }

    public void StartFlash(float counterTime = 0f)
    {

        IsActive = true;
        flashTimer = 0;
        countertimer = counterTime + 1;

    }

    public void EndFlash()
    {

        IsActive = false;
        WeakenedText.enabled = false;
        timeText.enabled = false;

    }

    void FixedUpdate()
    {

        if (IsActive)
        {

            countertimer -= Time.fixedDeltaTime;
            flashTimer -= Time.fixedDeltaTime;

            if(flashTimer <= 0)
            {

                flashTimer = flashTime;

                WeakenedText.enabled = displayed;
                timeText.enabled = displayed;

                if (displayed)
                {
                    timeText.text = ((int)countertimer).ToString();
                    AudioPlayer.playWeakBeep();
                }

                displayed = !displayed;

            }

        }

    }

    /*async void Flash()
    {

        if (IsActive)
        {
            WeakenedText.enabled = true;
            timeText.enabled = true;
            countertimer -= flashTime;
            timeText.text = ((int)countertimer).ToString();
            await Task.Delay(flashTimeMilliseconds);
            WeakenedText.enabled = false;
            timeText.enabled = false;
            countertimer -= flashTime;
            await Task.Delay(flashTimeMilliseconds);
            Flash();
        }
    }*/

    void OnDestroy()
    {
        IsActive = false;
        WeakenedText.enabled = false;
        timeText.enabled = false;
    }
}
