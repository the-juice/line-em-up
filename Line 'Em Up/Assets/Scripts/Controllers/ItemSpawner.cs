﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{

    public ItemSpawnPoint[] itemSpawners;

    void Start()
    {
    
        SpawnAllItems();

    }

    //This is called whenever an item is picked up at a spawn point
    //to let the spawner know to start the respawn timer
    public void ItemPickedUp(ItemSpawnPoint spawnPoint)
    {

        StartCoroutine(QueueItemToRespawn(spawnPoint));

    }

    IEnumerator QueueItemToRespawn(GameObject prefab, Vector3 position, float time)
    {

        yield return new WaitForSeconds(time);

        SpawnItem(prefab, position);

    }

    IEnumerator QueueItemToRespawn(ItemSpawnPoint spawnPoint)
    {

        yield return new WaitForSeconds(spawnPoint.spawnTime);

        SpawnItem(spawnPoint);

    }

    void SpawnAllItems()
    {

        foreach(ItemSpawnPoint spawner in itemSpawners)
        {

            SpawnItem(spawner);

        }

    }

    void SpawnItem(GameObject prefab, Vector3 pos)
    {

        GameObject go = SimplePool.Spawn(prefab, pos, Quaternion.identity);
        go.transform.SetParent(transform);
        Item item = go.GetComponent<Item>();

    }

    void SpawnItem(ItemSpawnPoint spawnPoint)
    {

        GameObject go = SimplePool.Spawn(spawnPoint.prefab, spawnPoint.position, Quaternion.identity);
        go.transform.SetParent(transform);
        Item item = go.GetComponent<Item>();
        item.spawner = this;
        item.spawnPoint = spawnPoint;

    }

}

[System.Serializable]
public struct ItemSpawnPoint
{

    public GameObject prefab;

    public float spawnTime;

    public Vector3 position;

}