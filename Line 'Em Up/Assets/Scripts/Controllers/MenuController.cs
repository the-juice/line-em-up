﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{

    public GameObject pauseMenu;

    public bool isPaused = false;

    void Start()
    {

        UnpauseGame();

    }

    public void TogglePause()
    {

        if (!isPaused)
        {
            PauseGame();
        }
        else
        {
            UnpauseGame();
        }

    }

    public void PauseGame()
    {
        isPaused = true;
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
    }

    public void UnpauseGame()
    {
        isPaused = false;
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
    }

    public void Quit()
    {
        Application.Quit();
    }

}
