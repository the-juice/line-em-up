﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using UnityEngine.SceneManagement;

public enum Team { None, Red, Green };

public class GameManager : MonoBehaviour {

    public static GameManager Instance;

    //The length of the match in seconds
    public int matchTime = 150;

    public GameObject[] players;

    Dictionary<Team, int> scores;

    //Action to be run when the score is updated (i.e. change the scoreboard)
    Action<int, int> UpdateScore;

    Action<float> UpdateTime;

    public GameObject gameoverText;
    public GameObject countdownText;

    public Color redColor;
    public Color greenColor;
    public Color drawColor;

    public AudioSource Sound321;
    public AudioSource SoundBegin;

    string[] counts = { "3", "2", "1" };

    void Awake() {

        Instance = this;

    }

    void Start() {

        NewGame();

    }

    IEnumerator Countdown() {

        countdownText.SetActive(true);

        Time.timeScale = 0;

        for (int i = 0; i < counts.Length; i++) {

            countdownText.GetComponentInChildren<TextMeshProUGUI>().text = counts[i];

            Sound321.Play();

            yield return new WaitForSecondsRealtime(0.5f);

        }

        countdownText.GetComponentInChildren<TextMeshProUGUI>().text = "Fight!";

        SoundBegin.Play();

        Time.timeScale = 1;

        StartCoroutine(MatchTime());

        yield return new WaitForSecondsRealtime(0.5f);

        countdownText.SetActive(false);

    }

    IEnumerator MatchTime() {

        for (int t = matchTime; t >= 0; t--) {

            if (UpdateTime != null) {
                UpdateTime(t);
            }

            yield return new WaitForSeconds(1f);

        }

        EndGame();

    }

    public void NewGame() {
    
        scores = new Dictionary<Team, int>();

        scores[Team.Red] = 0;
        scores[Team.Green] = 0;

        foreach (GameObject player in players) {

            SpawnController.Instance.Spawn(player);

        }

        if(UpdateScore != null) {
            UpdateScore(scores[Team.Red], scores[Team.Green]);
        }

        if (UpdateTime != null) {
            UpdateTime(matchTime);
        }

        gameoverText.SetActive(false);

        StartCoroutine(Countdown());

    }

    public void EndGame() {

        Debug.Log("Game Over");

        string text = "Draw";
        Color color = drawColor;

        //Pause game
        Time.timeScale = 0;

        //The match is over, determine the winner
        if (scores[Team.Red] > scores[Team.Green]) {

            //Team One wins
            Debug.Log("Red Team Wins");
            text = "Red Team Wins!";
            color = redColor;

        } else if (scores[Team.Red] < scores[Team.Green]) {

            //Team Two wins
            Debug.Log("Green Team Wins");
            text = "Green Team Wins!";
            color = greenColor;

        }
        else {

            //Otherwise, draw
            Debug.Log("Draw");
            text = "Draw";
            color = drawColor;

        }

        gameoverText.SetActive(true);
        gameoverText.GetComponentInChildren<TextMeshProUGUI>().text = "Game Over\n" + text;
        gameoverText.GetComponentInChildren<TextMeshProUGUI>().color = color;

        StartCoroutine(LoadMainMenu());

    }

    IEnumerator LoadMainMenu() {

        yield return new WaitForSecondsRealtime(5f);

        SceneManager.LoadScene(0);
        
    }

    public void IncrementScore(Team team, int score) {

        if (scores.ContainsKey(team)) {

            scores[team] += score;

            if(UpdateScore != null) {
                UpdateScore(scores[Team.Red], scores[Team.Green]);
            }

            //Debug.Log(scores);

        }

    }

    public void RegisterUpdateScore(Action<int, int> cb) {
        UpdateScore += cb;
    }

    public void RegisterUpdateTime(Action<float> cb) {
        UpdateTime += cb;
    }

}
