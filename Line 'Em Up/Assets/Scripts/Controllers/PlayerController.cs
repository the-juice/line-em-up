﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;

public class PlayerController : MonoBehaviour {

    //Mouse and keyboard controls:
    //Move with WASD
    //Rotate with left and right arrows
    //Zip to teammate with space
    //Use ability with left shift

    //This is how fast the player will accelerate when moving
    float movementAcceleration;
    public float defaultMovementAccel;

    //This is how fast the player will slow down when not moving
    public float movementDrag;

    //This is the maximum velocity in each direction the player can attain when not in the 'zip' state
    public float maxMovementVelocity;

    //This is a variable used to store the max movement and max zip velocity parameters
    private float maxVelocity;

    //This is the maximum velocity possible while weakened
    public float maxWeakenedVelocity;

    //How fast the player can rotate
    public float rotationSpeed;

    //How fast the player will accelerate towards their teammate when zipping
    public float zipAcceleration;

    //The "terminal velocity" of the player, can only be achieved when zipping
    public float maxZipVelocity;

    //The amount of knockback for enemy shots
    public float knockbackAmount;

    //The intensity of the dodge force
    public float dodgeAmount;

    //Reference to dodge bar - used to show when dodge is on cooldown
    public Slider DodgeBar;

    //Init variable for keeping track of terminal velocity, used when testing collisions with enemies
    bool terminalVelocity = false;

    //Variable for disabling terminal velocity while dodging
    bool canGoTerminal = true;

    //How long it takes for the zip to "charge"
    public float zipChargeTime;
    float zipTimer;
    private bool canZip = true;
    private bool startedZipping = false;

    //Refernece to the PlayerData
    [HideInInspector]
    public PlayerData playerData;

    //Reference to the object's rigidbody
    Rigidbody2D rb;

    //Reference to the line renderer, this will hopefully change in the future
    LineRenderer lr;

    //Reference to the weapon script
    Weapon weaponScript;

    //Reference to audio player
    public AudioScript AudioPlayer;

    //Reference to the two physics materials for movement and zipping
    public PhysicsMaterial2D movementMat;
    public PhysicsMaterial2D zipMat;

    //Terminal Velocity variables
    public SpriteRenderer termVelIndicator;
    Color defaultColor;
    public float terminalVelocityThreshold;

    //Holders for the objects that need to be rotated
    public Transform rotateWithBody;
    public Transform rotateWithLegs;

    //Animators for the legs
    public Animator walkingAnimator;

    public Animator deathAnimator;

    //Setting up variables for input
    public MenuController MenuController;
    private string moveHorizontal;
    private string moveVertical;
    private string lookHorizontal;
    private string lookVertical;
    private string inputZip;
    private string inputDodge;
    private string pause;
    public int controllerNumber;

    //Dodge variables
    private bool canDodge = true;

    //Reference to player sprite
    public SpriteRenderer sprite;
    Color spriteColor;
    public SpriteRenderer Gun;
    public SpriteRenderer Legs;

    void Start() {

        DodgeBar.value = 0f;

        SetControls();

        rb = GetComponent<Rigidbody2D>();
        rb.drag = movementDrag;

        maxVelocity = maxMovementVelocity;

        lr = GetComponentInChildren<LineRenderer>();

        zipTimer = zipChargeTime;

        weaponScript = GetComponent<Weapon>();

        spriteColor = sprite.color;

    }

    void SetControls() {
        //Set up string variables with this player's controller number
        moveHorizontal = "MoveHorizontal" + controllerNumber;
        moveVertical = "MoveVertical" + controllerNumber;
        lookHorizontal = "LookHorizontal" + controllerNumber;
        lookVertical = "LookVertical" + controllerNumber;
        inputZip = "Zip" + controllerNumber;
        inputDodge = "Dodge" + controllerNumber;
        pause = "Pause" + controllerNumber;

    }

    //For respawing
    public void Inititialize(bool isInitialSpawn = false)
    {
        becomeStrong();

        //Only do these actions when respawning, not the initial spawn at the beginning of the game
        if (!isInitialSpawn)
        {

            

        }

        sprite.enabled = true;
        Gun.enabled = true;
        Legs.enabled = true;

    }

    void FixedUpdate() {

        //Movement
        Vector2 movement = new Vector2(Input.GetAxisRaw(moveHorizontal), Input.GetAxisRaw(moveVertical)).normalized;
        if (movement != Vector2.zero) {

            //The player is moved by apply a force in the direction of movement so that there is a 
            //slight delay between the input and the action
            //The ForceMode is Impulse so that the movement is still very responsive.
            rb.AddForce(movement * movementAcceleration, ForceMode2D.Impulse);

            //Rotate legs in direction of walking
            //rotateWithLegs.rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(-1 * movement.x, movement.y) * Mathf.Rad2Deg));


        }

        //Clamping the movement velocity to the maxVelocity so that the player doesn't go too fast
        rb.velocity = Vector2.ClampMagnitude(rb.velocity, maxVelocity);

        //Apply velocity to walking animator
        walkingAnimator.SetFloat("Speed", movement.magnitude);

        //Rotation
        float xrotation = -1 * Input.GetAxis(lookHorizontal);
        float yrotation = -1 * Input.GetAxis(lookVertical);

        if (Mathf.Abs(xrotation) >= 0.5f || Mathf.Abs(yrotation) >= 0.5f)
        {

            if (!playerData.isZipping) //Disable fire while zipping
            {
                weaponScript.CanFire = true;
            }
            else
            {
                weaponScript.CanFire = false; //Makes sure to disable fire even if the player was already firing when zip began
            }

            //Create an angle based on the x and y values of the right stick, then set the transform rotation
            var angle = Mathf.Atan2(xrotation, yrotation) * Mathf.Rad2Deg;
            //8-DIRECTION AIMING
            /* float angle = 0f;
            if (Mathf.Abs(xrotation) == 1)
            {
                if (Mathf.Abs(yrotation) == 1)
                {
                    if (xrotation > 0)
                    {
                        if (yrotation > 0)
                        {
                            angle = 45;
                        }
                        else
                        {
                            angle = 135;
                        }
                    }
                    else if (yrotation > 0)
                    {
                        angle = 315;
                    }
                    else
                    {
                        angle = 225;
                    }
                }
                else
                {
                    if (xrotation > 0)
                    {
                        angle = 90;
                    }
                    else
                    {
                        angle = 270;
                    }
                }
            }
            else
            {
                if (yrotation < 0)
                {
                    angle = 180;
                }
            } */

            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
            //Rotate body
            //rotateWithBody.rotation = Quaternion.Euler(new Vector3(0, 0, angle));

        }
        else
        {
            weaponScript.CanFire = false;
        }


        //Zip
        if (Input.GetAxisRaw(inputZip) > 0 && canZip && !playerData.teammate.AmKilled) {
 
            if (!playerData.isZipping) {

                BeginZipCharge();

            }

            if (zipTimer <= 0) {

                if (!startedZipping) {
                    BeginZip(); //make beginzip a separate function to set movement speed and play sound

                }
                else {

                    Zip(); //once everything has been set up, start actually zipping
                }

            }
            else {

                zipTimer -= Time.fixedDeltaTime;
                lr.SetPositions(new Vector3[] { transform.position, playerData.teammate.transform.position });
                lr.startWidth += 0.007f;
                lr.endWidth += 0.007f;

            }

        }
        else {

            if (playerData.isZipping) {

                StartCoroutine(EndZip());

            }

        }

        //Dodge trigger
        if (Mathf.Abs((Input.GetAxisRaw(inputDodge))) > 0.5 && canDodge && !startedZipping) {
            StartCoroutine(Dodge());
            AudioPlayer.playDodge();
        }

        //Terminal velocity
        if (rb.velocity.magnitude >= terminalVelocityThreshold && canGoTerminal) {
            termVelIndicator.enabled = true;
            terminalVelocity = true;
        }
        else {
            termVelIndicator.enabled = false;
            terminalVelocity = false;

        }
//Pause input
        if (Input.GetAxisRaw(pause) > 0 && !MenuController.isPaused)
        {
            MenuController.TogglePause();
        }


    }

    void BeginZipCharge() {

        playerData.isZipping = true;
        lr.enabled = true;
        lr.SetPositions(new Vector3[] { transform.position, playerData.teammate.transform.position });
        lr.startWidth = 0f;
        lr.endWidth = 0f;
        AudioPlayer.playZip("charge");

    }

    void BeginZip() {
        startedZipping = true;
        AudioPlayer.playZip("release");
        setMovementMode("slippery");
    }

    void Zip() {

        Vector2 direction = playerData.teammate.transform.position - transform.position;
        rb.AddForce(direction / direction.magnitude * zipAcceleration, ForceMode2D.Impulse);
        rb.velocity = Vector2.ClampMagnitude(rb.velocity, maxZipVelocity);

        lr.SetPositions(new Vector3[] { transform.position, playerData.teammate.transform.position });

    }

    IEnumerator EndZip() {

        zipTimer = zipChargeTime;
        playerData.isZipping = false;
        lr.enabled = false;
        canZip = false;

        //If the player had started to zip, switch the variable and take care of movement
        //If they were still charging, stop the sound immediately and do nothing to movement
        if (startedZipping) {
            startedZipping = false;
            //If they were already zipping, reduce movement speed, wait 0.5s, then switch back to regular movement
            movementAcceleration = 0.00001f;
            //await Task.Delay(500);
            yield return new WaitForSeconds(.5f);
            setMovementMode("regular");

        }
        else {
            AudioPlayer.stopZip();
        }

        //Wait another 0.5 seconds to activate Zip
        //await Task.Delay(500);
        yield return new WaitForSeconds(.5f);
        if (!playerData.AmWeakened) //If the player is weakened, do not re-enable canZip
        {
            canZip = true;
        }

    }

    //Dodge move
    IEnumerator Dodge() {
        canDodge = false;
        canGoTerminal = false;
        playerData.CanTakeDamage = false;

        movementAcceleration = 0.0f; //disable input while the dodge is happening
        maxVelocity = maxZipVelocity;

        Vector2 forceVector = new Vector2(Input.GetAxisRaw(moveHorizontal), Input.GetAxisRaw(moveVertical)).normalized;
        if (forceVector == Vector2.zero)//If the player is not moving, dodge in the direction that they're facing
        {
            Vector2 forceAdded = rb.transform.up * dodgeAmount * rb.mass; //Multiply by mass so that the force is not too large when the player is in "slippery" mode
            rb.AddForce(forceAdded, ForceMode2D.Impulse);
            //await Task.Delay(100);
            yield return new WaitForSeconds(.1f);
            if (!startedZipping)
            {
                rb.AddForce(forceAdded * -.5f, ForceMode2D.Impulse);
            }
            if (!startedZipping)
            {
                rb.AddForce(forceAdded * -.5f, ForceMode2D.Impulse);
            }
        }
        else //If the player is moving, dodge in the direction of input
        {
            Vector2 forceAdded = forceVector * dodgeAmount * rb.mass;
            rb.AddForce(forceAdded, ForceMode2D.Impulse);
            //await Task.Delay(100);
            yield return new WaitForSeconds(.1f);
            if (!startedZipping)
            {
                rb.AddForce(forceAdded * -.5f, ForceMode2D.Impulse);
            }
        }
        if (!startedZipping)
        {
            movementAcceleration = defaultMovementAccel;
            maxVelocity = maxMovementVelocity;
        }
        canGoTerminal = true;
        playerData.CanTakeDamage = true;


        int i = 1;
        while (i <= 20) //show dodge bar fill animation
        {
            DodgeBar.value = i;
            //await Task.Delay(100);
            yield return new WaitForSeconds(.1f);
            i += 1;
        }
        i = 0;
        DodgeBar.value = i;

        if (!playerData.AmWeakened) //If the player is weakened, do not re-enable canDodge
        {
            canDodge = true;
        }

    }


    //Handle collisions: if colliding with teammate, end zip. Based on collision speed, shake screen and play sound
    void OnCollisionEnter2D(Collision2D collision) {
        //True if the player collides with an enemy
        bool hitEnemy = false;
        //The amount of collision damage the enemy will take (if any, not sure if this is a feature)
        // int damage = 0; - I don't think we'll need to use this ~Jonah 

        if (collision.gameObject.tag == "Player") {

            if (collision.gameObject != playerData.teammate.gameObject) {
                hitEnemy = true;
            }
            else {
                StartCoroutine(EndZip());
            }
        }

        if (collision.relativeVelocity.magnitude >= 44) {

            Metadesc.CameraShake.ShakeManager.I.AddShake("ShakeLarge");
            AudioPlayer.playImpact("large");
        }
        else if (collision.relativeVelocity.magnitude > 30) {
            Metadesc.CameraShake.ShakeManager.I.AddShake("ShakeMedium");
            AudioPlayer.playImpact("medium");
        }
        else if (collision.relativeVelocity.magnitude > 20) {
            Metadesc.CameraShake.ShakeManager.I.AddShake("ShakeSmall");
            AudioPlayer.playImpact("small");
        }
        else if (collision.relativeVelocity.magnitude > 10) {
            AudioPlayer.playImpact("tiny");
        }

        if (hitEnemy) {
            if (terminalVelocity) {
                //Kill the enemy
                collision.gameObject.GetComponent<PlayerData>().Kill();
                //Play Death Sound
                AudioPlayer.playDeathExplosion();

                //Increment score
                //GameManager.Instance.IncrementScore(playerData.Team, 1);

            }
            else {
                //Take collision damage???
            }
        }

    }

    public void TakeDamage(Vector2 damageVector)
    {
        StartCoroutine(TakeDamageCo(damageVector));
    }

    IEnumerator TakeDamageCo(Vector2 damageVector) {

        //Take the vector given by the bullet script through the playerdata script and apply a knockback
        AudioPlayer.playDamageTaken();
        if (rb.mass == 1.0f)
        {
            rb.AddForce(knockbackAmount * damageVector);
        }
        sprite.color = Color.gray;
        yield return new WaitForSeconds(.1f);
        sprite.color = spriteColor;
        
    }

    public void becomeWeakened() {
        StartCoroutine(EndZip());
        canZip = false;
        canDodge = false;
        weaponScript.fireTime = 1.0f / (weaponScript.fireRate / 2.0f);
        weaponScript.fireTimer = weaponScript.fireTime;
        setMovementMode("weakened");
        AudioPlayer.playWeakenedWail();
    }

    public void becomeStrong() {
        canZip = true;
        canDodge = true;
        weaponScript.fireTime = 1.0f / weaponScript.fireRate;
        weaponScript.fireTimer = weaponScript.fireTime;
        setMovementMode("regular");
    }

    public void setMovementMode(string movementMode) {
        if (movementMode == "regular") {
            rb.mass = 1.0f;
            movementAcceleration = defaultMovementAccel;
            rb.drag = movementDrag;
            rb.sharedMaterial = movementMat;
            maxVelocity = maxMovementVelocity;
        }

        if (movementMode == "slippery") {
            rb.sharedMaterial = zipMat;
            maxVelocity = maxZipVelocity; //Added because when applying movement input, the regular max velocity was still applying a velocity clamp
            rb.mass = 0.0001f; //Set mass very low to avoid launching players

            //Switch to an alternate movement style with low drag and movement acceleration
            movementAcceleration = 0.00015f;
            rb.drag = 2f;
        }

        if (movementMode == "weakened") {
            maxVelocity = maxWeakenedVelocity;
        }
    }

    public void Kill()
    {
        StartCoroutine(TimedKill());
    }

    IEnumerator TimedKill() {
        deathAnimator.GetComponent<SpriteRenderer>().enabled = true;
        deathAnimator.SetTrigger("Die");
        //yield return new WaitForSeconds(deathAnimator.GetCurrentAnimatorClipInfo(0)[0].clip.length);
        yield return new WaitForSeconds(0.3585f);
        sprite.enabled = false;
        Gun.enabled = false;
        Legs.enabled = false;
        yield return new WaitForSeconds(0.1f);
        //deathAnimator.ResetTrigger("Die");
        deathAnimator.GetComponent<SpriteRenderer>().enabled = false;
        
        SpawnController.Instance.Respawn(gameObject);
    }

    public void CleanUpForRespawn()
    {
        lr.enabled = false;
        DodgeBar.value = 0;
    }

}
