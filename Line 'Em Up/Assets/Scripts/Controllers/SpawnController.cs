﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour {

    public static SpawnController Instance;

    public float respawnTime = 4f;

    List<Transform> spawnPoints;

    int nextSpawnPoint = 0;

    Queue<GameObject> playersToRespawn;
    Queue<float> respawnTimers;

    void Start() {

        Instance = this;

        playersToRespawn = new Queue<GameObject>();
        respawnTimers = new Queue<float>();

        spawnPoints = new List<Transform>();
        foreach(Transform t in transform) {
            spawnPoints.Add(t);
        }


    }

    void FixedUpdate() {

        if(respawnTimers.Count > 0 && respawnTimers.Peek() <= Time.fixedTime) {

            respawnTimers.Dequeue();
            SetSpawn(playersToRespawn.Dequeue());

        }

        /*for (int i = 0; i < respawnTimers.Count; i++) {

            respawnTimers[i] -= Time.fixedDeltaTime;

            if(respawnTimers[i] <= 0) {

                //Respawn this player
                SetSpawn(playersToRespawn[i]);

                respawnTimers.RemoveAt(i);
                playersToRespawn.RemoveAt(i);
                
                i--;

            }

        }*/

    }

    //This is the initial spawn at the beginning of the game
    public void Spawn(GameObject player) {

        player.transform.position = spawnPoints[nextSpawnPoint].position;

        //Debug.Log("Spawned " + player.name + " at " + spawnPoints[nextSpawnPoint]);

        nextSpawnPoint = (nextSpawnPoint + 1) % spawnPoints.Count;

    }

    //This is the spawn to be used after a player is killed
    public void Respawn(GameObject player) {

        player.GetComponent<PlayerController>().CleanUpForRespawn();

        player.SetActive(false);

        QueuePlayer(player);

    }

    void QueuePlayer(GameObject player)
    {

        playersToRespawn.Enqueue(player);
        respawnTimers.Enqueue(Time.fixedTime + respawnTime);

    }

    void SetSpawn(GameObject player) {

        //Find the farthest spawn point from any other player
        float[] spawnDistances = new float[spawnPoints.Count];
        for (int i = 0; i < spawnDistances.Length; i++)
        {
            spawnDistances[i] = float.MaxValue;
        }

        for (int i = 0; i < spawnPoints.Count; i++)
        {
            foreach (GameObject otherPlayer in GameManager.Instance.players)
            {

                if (otherPlayer.activeSelf && otherPlayer != player)
                {

                    //float distance = Vector2.SqrMagnitude(otherPlayer.transform.position - spawnPoints[i].position);
                    float distance = Vector2.Distance(otherPlayer.transform.position, spawnPoints[i].position);
                    if (distance < spawnDistances[i])
                    {

                        spawnDistances[i] = distance;

                    }

                }

            }
        }

        float maxDistance = 0;
        int spawnPointIndex = 0;
        for (int i = 0; i < spawnDistances.Length; i++) {

            if(spawnDistances[i] > maxDistance)
            {
                maxDistance = spawnDistances[i];
                spawnPointIndex = i;
            }

        }

        //At this point we know where the farthest spawn point from any player is, now spawn the player there
        player.transform.position = spawnPoints[spawnPointIndex].position;
        player.SetActive(true);
        player.GetComponent<PlayerData>().Initialize();

    }


}
