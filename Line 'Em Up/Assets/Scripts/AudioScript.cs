﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour
{
    //Instantiate all the varibles for the clips and audio sources
    public AudioSource bullet;
    public AudioSource damageTaken;
    public AudioSource DeathExplosion;
    public AudioSource Impact;
    public AudioSource Zip;
    public AudioSource weakenedWail;
    public AudioSource dodge;
    public AudioSource healthPack;
    public AudioSource weakBeep;
    public AudioSource deathTransient;
    public AudioClip largeImpact;
    public AudioClip medImpact;
    public AudioClip smallImpact;
    public AudioClip tinyImpact;
    public AudioClip zipCharge;
    public AudioClip zipRelease;


    // Create a function for each sound to be called
    public void playBullet()
    {
        bullet.Play();
    }

    public void playDamageTaken()
    {
        damageTaken.Play();
    }

    public void playDeathExplosion()
    {
        DeathExplosion.Play();
    }

    public void playWeakenedWail()
    {
        weakenedWail.Play();
    }

    public void playDodge()
    {
        dodge.Play();
    }

    public void playImpact(string ImpactSize)
    {
        if (ImpactSize == "large") {
            Impact.clip = largeImpact;
            Impact.Play();
        } else if (ImpactSize == "medium")
        {
            Impact.clip = medImpact;
            Impact.Play();
        } else if (ImpactSize == "small")
        {
            Impact.clip = smallImpact;
            Impact.Play();
        }
        else if (ImpactSize == "tiny")
        {
            Impact.clip = tinyImpact;
            Impact.Play();
        }
    }

    public void playZip(string chargeOrRelease)
    {
        if (chargeOrRelease == "charge")
        {
            Zip.clip = zipCharge;
            Zip.Play();
        } else if (chargeOrRelease == "release")
        {
            Zip.clip = zipRelease;
            Zip.Play();
        }
    }
    
    public void stopZip()
    {
        Zip.Stop();
    }

    public void playHealthSound()
    {
        healthPack.Play();
    }

    public void playWeakBeep()
    {
        weakBeep.Play();
    }

    public void playDeathTransient()
    {
        deathTransient.Play();
    }
}
